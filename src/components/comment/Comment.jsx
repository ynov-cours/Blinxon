import React from 'react';
import ReactMarkdown from 'react-markdown';
import gfm from 'remark-gfm';
import emoji from 'remark-emoji';
import './comment.scss';

function Comment(props){
  return(
    <div className="comment col-12 col-lg-3 mx-3 p-2 d-flex flex-column justify-content-between my-3">
      <div className="comment-text-container d-flex flex-row justify-content-center">
        <ReactMarkdown plugins={[gfm, emoji]} className="comment-text" children={props.text}/>
      </div>
      <div className="d-flex flex-row justify-content-between">
        <p className="comment-infos">{props.author} - {props.date}</p>
        {
          props.approuved === 1 ?
          <div className="d-flex flex-row align-items-center">
            <p className="comment-infos-small p-0 m-0 me-1">Liké par Blinxon</p>
            <img className="p-0 m-0" src="/img/logos/LOGO_BLINXON_SVG.svg" alt="Logo de l'association Blinxon" height="25px"/>
          </div> :
          ""
        }
      </div>
    </div>
  )
}

export default Comment;

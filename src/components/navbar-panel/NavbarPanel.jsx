import React from 'react';
import './navbarpanel.scss';


function NavbarPanel(){
  return(
    <nav id="navbarpanel" className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid p-2">
        <a className="navbar-brand d-flex flex-row" href="/">
          <img src="/img/logos/LOGO_BLINXON_SVG.svg" alt="Logo de l'association Blinxon" height="50" className="mt-1 navbar-brand-image"/>
        </a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div className="navbar-nav">
            <a className="nav-link navbar-panel-link mx-2 disable special mt-3 mt-lg-0" href="/panel">Administration |</a>
            <a className="nav-link navbar-panel-link mx-2" href="/panel">Accueil</a>
            <a className="nav-link navbar-panel-link mx-2" href="/panel/articles">Articles</a>
            <a className="nav-link navbar-panel-link mx-2" href="/panel/live-panel">Live-panels</a>
            <a className="nav-link navbar-panel-link mx-2" href="/panel/texts">Textes</a>
            <a className="nav-link navbar-panel-link mx-2" href="/panel/stats">Statistiques</a>
            <a className="nav-link navbar-panel-link mx-2" href="/panel/comments">Commentaires</a>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default NavbarPanel;

import React from 'react';
import './live-panel.scss';

function PagePanel(props){

  const large_panel_style = {
    panel: {
      backgroundImage: `url(data:image/png;base64,${props.panel_image})`,
      backgroundPosition: "top",
    },
    blinxon_page_panel_title: {
      fontSize: "3em"
    },
    blinxon_page_panel_subtitle: {
      fontSize: "1.2em"
    }
  };

  const normal_panel_style = {
    panel: {
      backgroundImage: `url(data:image/png;base64,${props.panel_image})`,
    },
    blinxon_page_panel_title: {
      fontSize: "2em"
    }
  };

  const panelStyles = () => {
    if(props.panel_mode === "normal"){
      return normal_panel_style;
    }else if(props.panel_mode === "large") {
      return large_panel_style;
    }else{
      return normal_panel_style;
    }
  };

  return(
    <a id="livePanel" href={props.panel_link ? props.panel_link : "/"} style={panelStyles().panel} className={`my-3 p-3 d-flex flex-column justify-content-end`}>
      <div>
        <p className="blinxon-live-panel-title" style={panelStyles().blinxon_page_panel_title}>{props.panel_title}</p>
        {
          props.panel_subtitle ?
          <p style={panelStyles().blinxon_page_panel_subtitle} className="blinxon-live-panel-subtitle">{props.panel_subtitle}</p>
          :
          ""
        }
      </div>
    </a>
  )
}

export default PagePanel;

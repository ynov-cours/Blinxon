import React, {useState, useEffect} from 'react';
import Navbar from '../../components/navbar/Navbar';
import ReactMarkdown from 'react-markdown';
import gfm from 'remark-gfm';
import emoji from 'remark-emoji';
import config from '../../config';
import * as FIcons from 'react-icons/fi';
import './articlefull.scss';

function ArticleFull(props){

  const [article, setArticle] = useState([]);

  useEffect(() => {
    getArticle(props.match.params.id);
  }, [props.match.params.id]);

  const getArticle = (id) => {
    fetch(`${config.API_URL}/articles/${id}`)
    .then(response => response.json())
    .then(response => setArticle(response));
  }

  return(
    <div id="articlefull" className="d-flex flex-column align-items-center mb-5">
      <div className="col-12">
        <Navbar/>
      </div>
      <div className="col-10 pt-5 article-container">
        <a href="/nos-actualites" className="p-0 m-0 go-back-link"><FIcons.FiChevronLeft/> Retour aux articles</a>
        <p className="p-0 m-0 article-title"><ReactMarkdown plugins={[gfm, emoji]} children={article.title}/></p>
        <p className="p-0 m-0 article-subtitle">Posté par <strong>{article.author}</strong> le <strong>{article.article_date_formated}</strong></p>

        <div className="col-12">
          <img src={`data:image/plain;base64,${article.image_base64}`} alt={`Illustration de l'article n°${article.id}`}/>
        </div>
        <ReactMarkdown plugins={[gfm, emoji]} className="mt-5 article-content" children={article.content}/>
      </div>
    </div>
  )
}

export default ArticleFull;

import React from 'react';
import * as BIcons from 'react-icons/bs';
import './navbar.scss';

function Navbar(props){
  return(
    <nav id="blinxon-navbar" className="navbar navbar-expand-lg sticky-top">
      <div className="container-fluid d-flex flex-lg-column px-3 px-lg-0">
        <a className="navbar-brand d-flex flex-row" href="/">
          <img src="/img/logos/LOGO_BLINXON_SVG.svg" alt="Logo de l'association Blinxon" height="70" className="mt-1 navbar-brand-image"/>
          <p className="blinxon-navbar-title p-0 m-0 ms-2 mt-1 mt-lg-0">Blinxon</p>
        </a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavLinksContainer" aria-controls="navbarNavLinksContainer" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavLinksContainer">
          <div className="navbar-nav">
            <a className="nav-link mx-md-3 d-flex d-lg-block flex-row align-items-center" href="/"><span className="d-flex d-lg-none"><BIcons.BsArrowRightShort/></span>Accueil</a>
            <a className="nav-link mx-md-3 d-flex d-lg-block flex-row align-items-center" href="/notre-histoire"><span className="d-flex d-lg-none"><BIcons.BsArrowRightShort/></span>Le projet</a>
            <a className="nav-link mx-md-3 d-flex d-lg-block flex-row align-items-center" href="/nous-aider"><span className="d-flex d-lg-none"><BIcons.BsArrowRightShort/></span>Nous aider</a>
            <a className="nav-link mx-md-3 d-flex d-lg-block flex-row align-items-center" href="/nos-actualites"><span className="d-flex d-lg-none"><BIcons.BsArrowRightShort/></span>Nos actualités</a>
            <a className="nav-link mx-md-3 d-flex d-lg-block flex-row align-items-center" href="/notre-progression"><span className="d-flex d-lg-none"><BIcons.BsArrowRightShort/></span>Notre progression</a>
            <a className="nav-link mx-md-3 d-flex d-lg-block flex-row align-items-center" href="/espace-discussion"><span className="d-flex d-lg-none"><BIcons.BsArrowRightShort/></span>Discussion</a>
            <a className="nav-link mx-md-3 d-flex d-lg-block flex-row align-items-center" href="/nous-contacter"><span className="d-flex d-lg-none"><BIcons.BsArrowRightShort/></span>Nous contacter</a>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Navbar;

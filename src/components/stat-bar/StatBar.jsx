import React from 'react';
import './statbar.scss';

function StatBar(props){
  return(
    <div className="statbar col-10 mb-5">
      <div>
        <p className="statbar-title">{props.title}</p>
        <p className="statbar-description">{props.description}</p>
      </div>
      <div className="progress mt-2" style={{height: "10px"}}>
        <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{width: `${props.percentage}%`, backgroundColor: props.color}} aria-valuenow={props.percentage} aria-valuemin={props.min} aria-valuemax={props.max}>{props.label ? props.label : ""}</div>
      </div>
      <div className="d-flex flex-row justify-content-between">
        <p>{props.min} {props.unit}</p>
        <p>{props.max} {props.unit}</p>
      </div>
      <div className="d-flex flex-row mt-2">
        <p className="me-2">Liens associés :</p>
        {
          props.links_name
          ?
          props.links_name.map((link, link_index) => {
            return(
              <div className="d-flex flex-row">
                <a key={link_index} href={props.links_url[link_index]} className="statbar-link">{link}</a>
                {
                  link_index === props.links_name.length - 1 ? "" : <p className="mx-2">/</p>
                }
              </div>
            )
          })
          :
          ""
        }
      </div>
    </div>
  )
}

export default StatBar;

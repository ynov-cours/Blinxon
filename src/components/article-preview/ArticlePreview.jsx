import React from 'react';
import ReactMarkdown from 'react-markdown';
import gfm from 'remark-gfm';
import emoji from 'remark-emoji';
import './articlepreview.scss';

function ArticlePreview(props){

  const articlePreview = {
    backgroundImage: `url(data:image/png;base64,${props.image})`,
  }

  return(
    <a href={`/article/${props.id}`} className="articlepreview my-3 p-0 d-flex flex-row">
      <div className="article-preview-content d-flex flex-column justify-content-between p-2 col-8">
        <div>
          <p className="article-preview-title"><ReactMarkdown plugins={[gfm, emoji]} children={props.title}/></p>
          <p className="article-preview-infos">Posté par {props.author}</p>
          <p className="article-preview-content mt-3"><ReactMarkdown plugins={[gfm, emoji]} children={props.description}/></p>
        </div>
        <div className="d-flex flex-row justify-content-between">
          <p className="article-preview-infos">Posté le : {props.date}</p>
          <p className="article-preview-infos">Catégorie : {props.category}</p>
        </div>
      </div>
      <div style={articlePreview} className="article-preview-background col-4 m-0 p-0">

      </div>
    </a>
  )
}

export default ArticlePreview;

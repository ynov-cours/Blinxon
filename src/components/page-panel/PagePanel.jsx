import React from 'react';
import './page-panel.scss';

function PagePanel(props){

  const large_panel_style = {
    panel: {
      backgroundImage: `url(img/panels/${props.panel_image})`,
      // width: "400px !important"
    },
    blinxon_page_panel_title: {
      fontSize: "3em"
    },
    blinxon_page_panel_subtitle: {
      fontSize: "1.2em"
    }
  };

  const normal_panel_style = {
    panel: {
      backgroundImage: `url(img/panels/${props.panel_image})`,
      // width: "200px"
    },
    blinxon_page_panel_title: {
      fontSize: "2em"
    }
  };

  const panelStyles = () => {
    if(props.panel_mode === "normal"){
      return normal_panel_style;
    }else if(props.panel_mode === "large") {
      return large_panel_style;
    }else{
      return normal_panel_style;
    }
  };

  return(
    <a href={props.panel_link ? props.panel_link : "/"} style={panelStyles().panel} className={`my-3 p-3 d-flex flex-column justify-content-end ${props.panel_mode === "normal" ? "blinxon-page-panel" : "blinxon-page-panel blinxon-page-panel-large"}`}>
    <div>
      <p className="blinxon-page-panel-title" style={panelStyles().blinxon_page_panel_title}>{props.panel_title}</p>
      {
        props.panel_subtitle ?
        <p style={panelStyles().blinxon_page_panel_subtitle} className="blinxon-page-panel-subtitle">{props.panel_subtitle}</p>
        :
        ""
      }
    </div>
  </a>
)
}

export default PagePanel;

import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// Website - Public side
import Navbar from './components/navbar/Navbar';
import Home from './pages/home/Home';
import Histoire from './pages/histoire/Histoire';
import ArticleFull from './components/article/ArticleFull';
import ActualitesPage from './pages/actus/ActualitesPage';
import ProgressionPage from './pages/progression/Progression';
import Discussion from './pages/discussion/Discussion';
import Help from './pages/help/Help';
import Contact from './pages/contact/Contact';

// Website - Admin side
import Panel from './pages/panel/Panel';
import PanelArticles from './pages/panel-articles/PanelArticles';
import PanelArticleEdit from './pages/panel-article-edit/PanelArticleEdit';
import PanelArticleAdd from './pages/panel-article-add/PanelArticleAdd';
import PanelHomeCards from './pages/panel-home-cards/PanelHomeCards';
import PanelHomeCardsAdd from './pages/panel-home-cards-add/PanelHomeCardsAdd';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={props =>
          <div>
            <Navbar/>
            <Home/>
          </div>
        }/>

        <Route path="/home" component={props =>
          <div>
            <Navbar/>
            <Home/>
          </div>
        }/>

        <Route path="/notre-histoire" component={props =>
          <div>
            <Navbar/>
            <Histoire dates={[
              {
                reversed: false,
                date: "1998",
                text: "Premier départ",
                description: [
                  "En 1997, un homme épris de voyages et d’aventures se lance dans un nouveau défi : parcourir le désert marocain en Renault 4L. Le 4L Trophy est né ! ",
                  "En février 1998, ce sont 3 binômes qui partent alors à l’aventure dans les terres marocaines. 25 ans plus tard, en février 2022, nous serons plus de 3 000 jeunes et près de 1 500 4L, pour nous lancer à notre tour dans l’aventure 4L Trophy. En 10 jours, chaque équipage devra parcourir les 6 000 km de cette palpitante course d’orientation dont le village départ est situé à Biarritz, dans le sud-ouest de la France, et dont l’arche d’arrivée se tiendra à Marrakech au Maroc. En passant par l’Espagne et le désert du Sahara, chaque binôme aura ainsi l’occasion de s’orienter efficacement à l’aide de road book fourni par l’organisation et de s’entraider pour déloger les 4L ensablées ! En effet, vitesse et compétition n’ont pas leur place au bivouac des trophistes et laissent leur place à l’orientation et à la solidarité !"
                ]
              },
              {
                reversed: true,
                date: "2005",
                text: "Création EDD",
                description: [
                  "Lors d’un détour par le bivouac de Merzouga, les trophistes auront l’occasion de rencontrer les bénévoles de l’association Enfants du désert. Cette association a été créée en 2005 par Laeticia et Emmanuel Chevalier. Leur but : Faciliter aux enfants vivant dans des lieux reculés du désert un accès de qualité à l’éducation. ",
                  "Leur devise : 'Apprendre à lire, écrire, compter : un droit pour tous !'. Ce bivouac est l’un des évènements clés du voyage puisqu’il s’agira de remettre à l’association les dons de fournitures scolaires et sportives que chaque équipage aura pris soin de transporter depuis son départ de la France. Véritable engagement solidaire, la rencontre avec cette association sera l’un des moments forts de l’aventure. "
                ]
              },
              {
                reversed: false,
                date: "2009",
                text: "Croix rouge",
                description: [
                  "Depuis 2011, la Croix Rouge française est également partenaire du raid 4L Trophy. Elle a pour but de récolter auprès des équipages volontaires des denrées alimentaires sur le village départ à Biarritz afin de les redistribuer à des familles nécessiteuses françaises. De plus, des médecins volontaires de la Croix Rouge sont présents sur les bivouacs tout au long de l’aventure pour soutenir les équipes médicales de l’organisation. "
                ]
              },
              {
                reversed: true,
                date: "2015 - 2018",
                text: "Notre rencontre",
                description: [
                  "En ce qui concerne notre petite équipe, nous nous appelons Enzo et Olivia. Nous avons respectivement 20 et 21 ans et nous nous sommes rencontrés au lycée où nous sommes immédiatement devenus inséparables. Passionnés d’aventures et de découvertes, il a été évident que nous devions participer ensemble à cette aventure dès que nous avons découvert son existence. Après plusieurs années de préparation, nous voilà prêts à embarquer pour cette grande aventure !"
                ]
              },
              {
                reversed: false,
                date: "22 Janvier 2020",
                text: "Inscription",
                description: [
                  "Le 22 janvier 2020, lors d’une soirée des plus classiques, notre aventure démarre officiellement : sur un coup de tête, nous décidons de nous inscrire, là, maintenant, tout de suite. Quelques minutes plus tard, nous sommes inscrits et peu de temps après notre numéro d’équipage nous est attribué. Nous serons l’équipage 66. Nous sommes fiers et avons hâte de continuer à travailler pour atteindre notre objectif !"
                ]
              },
              {
                reversed: true,
                date: "2021",
                text: "Aujourd'hui",
                description: [
                  "Après une année passablement compliquée et un départ repoussé d’un an, nous sommes aujourd’hui plus motivés que jamais ! Notre association a été créée, les recherches de sponsors se poursuivent avec toujours plus de motivation et nous approchons petit à petit de notre objectif financier. La 4L se porte bien et continue sa préparation tandis que nous faisons connaissance avec nos futurs coéquipiers trophistes. Et nous attendons avec impatience de nous lancer sur les pistes marocaines ! "
                ]
              },
              {
                reversed: false,
                date: "2022",
                text: "Notre départ",
                description: [
                  "Le 17 février prochain, nous serons devant l’arche de départ du 4L Trophy, plus près que jamais à entamer un périple sportif et humain qui sera, sans aucun doute, inoubliable ! Nous espérons en revenir plus fort et plus émerveillés que jamais."
                ]
              }
            ]}/>
          </div>
        }/>
        <Route exact path="/nous-aider" component={Help}/>
        <Route exact path="/nos-actualites" component={ActualitesPage}/>
        <Route exact path="/notre-progression" component={ProgressionPage}/>
        <Route exact path="/espace-discussion" component={Discussion}/>
        <Route exact path="/nous-contacter" component={Contact}/>
        <Route exact path="/article/:id" component={ArticleFull}/>


        <Route exact path="/panel" component={Panel}/>
        <Route exact path="/panel/articles" component={PanelArticles}/>
        <Route exact path="/panel/articles/add" component={PanelArticleAdd}/>
        <Route exact path="/panel/articles/edit/:id" component={PanelArticleEdit}/>
        <Route exact path="/panel/live-panel" component={PanelHomeCards}/>
        <Route exact path="/panel/live-panel/add" component={PanelHomeCardsAdd}/>
      </Switch>
    </Router>
  );
}

export default App;

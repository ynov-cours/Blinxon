import React, { useState, useEffect } from 'react';
import NavbarPanel from '../../components/navbar-panel/NavbarPanel';
import LivePanel from '../../components/live-panel/LivePanel';
import axios from 'axios';
import config from '../../config.js';
import '../panel-home-cards-add/panelhomecardsadd.scss';

function PanelHomeCardsAdd(props){

  const [existingPanels, setExistingPanels] = useState([]);
  const [title, setTitle] = useState("");
  const [subtitle, setSubtitle] = useState("");
  const [image, setImage] = useState("");

  useEffect(() => {
    updatePanelsList();
  }, []);

  const updatePanelsList = () => {
    fetch(`${config.API_URL}/panels/all`)
    .then(response => response.json())
    .then(response => setExistingPanels(response));
  }

  const addPanel = (e) => {
    e.preventDefault();

    let panel = new FormData();
    panel.append('title', title);
    panel.append('subtitle', subtitle);
    panel.append('image', image)

    axios.post(`${config.API_URL}/panels/add?token=${config.TOKEN}`, panel, {headers: {"Content-Type": "multipart/form-data"}})
    .then(function(response){
      // Success stuff
      let feedback = document.getElementById('add_feedback');
      feedback.style.color = "#0F7322";
      feedback.innerHTML = "Panel ajouté avec succès !";
      updatePanelsList();
      document.getElementById('add_article_form').reset();

      setTimeout(function(){
        feedback.innerHTML = "";
      },5000);
    })
    .catch(function(err){
      // Error stuff
      let feedback = document.getElementById('add_feedback');
      feedback.style.color = "#e74c3c";
      feedback.innerHTML = "Une erreur est survenue...";
      updatePanelsList();

      setTimeout(function(){
        feedback.innerHTML = "";
      },5000);
    })
  }


  return(
    <div id="panelhomecardsadd" className="d-flex flex-column">
      <div>
        <NavbarPanel/>
      </div>
      <div className="d-flex flex-column flex-lg-row">
        <div className="panel-edit-form-container col-12 col-lg-3 p-3 px-5 border-right">
          <p className="panel-edit-article-title">Panel d'ajout</p>
          <form id="add_article_form" onSubmit={addPanel}>
            <div className="form-group mt-3">
              <label htmlFor="exampleInputEmail1">Titre</label>
              <input required onChange={(e) => setTitle(e.target.value)} type="text" className="form-control" id="inputTitle" placeholder="Titre du panel"/>
            </div>
            <div className="form-group mt-3">
              <label htmlFor="inputAuthor">Sous-titre</label>
              <input required onChange={(e) => setSubtitle(e.target.value)} type="text" className="form-control" id="inputSubtitle" placeholder="Sous titre du panel"/>
            </div>
            <div class="form-group mt-3">
              <label for="exampleFormControlFile1">Example file input</label>
              <input onChange={(e) => setImage(e.target.files[0])} type="file" class="form-control-file" id="exampleFormControlFile1"/>
            </div>
            <button type="submit" className="btn btn-outline-success mt-3 p-1">Ajouter l'article</button>
            <p id="add_feedback"></p>
          </form>

          <div className="mt-5">
            <p><strong>Liste des articles déjà existants :</strong></p>
            {
              existingPanels.length > 0
              ?
              existingPanels.map((item, index) => {
                return(
                  <p>{item.title}</p>
                )
              })
              :
              "Aucun panel"
            }
          </div>
        </div>
        <div className="panel-edit-article-preview-container col-12 col-lg-8 p-3">
          <LivePanel
            panel_title={title ? title : "No title"}
            panel_subtitle={subtitle ? subtitle : "No subtitle"}
            panel_image={image ? URL.createObjectURL(image) : ""}
          />
        </div>
      </div>
    </div>
  )
}

export default PanelHomeCardsAdd;

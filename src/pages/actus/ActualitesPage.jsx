import React, {useState, useEffect} from 'react';
import Navbar from '../../components/navbar/Navbar';
import ArticlePreview from '../../components/article-preview/ArticlePreview';
import config from '../../config';
import './actuspage.scss';

function ActualitesPage(props){

  const [articles, setArticles] = useState([]);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    getArticles();
    getCategories();
  }, []);

  const getArticles = () => {
    fetch(`${config.API_URL}/articles/all`)
    .then(response => response.json())
    .then(response => setArticles(response));
  }

  const getCategories = () => {
    fetch(`${config.API_URL}/articles/categories`)
    .then(response => response.json())
    .then(response => setCategories(response));
  }

  return(
    <div id="actuspage" className="d-flex flex-column align-items-center">
      <div className="col-12"><Navbar/></div>
      <div className="col-10 d-flex flex-column flex-lg-row pt-5">
        <div className="col-12 col-lg-3 d-flex flex-column align-items-center">
          <div className="sticky-top actus-menu col-12 d-flex flex-column">
            <form>
              <div className="form-group d-flex flex-column align-items-center">
                <div className="col-sm-10">
                  <label htmlFor="inputCategory">Trier par catégorie</label>
                  <select id="inputCategory" className="form-select form-select-lg p-1">
                    <option defaultValue>Pas de filtre sélectionné</option>
                    {
                      categories.map((item, index) => {
                        return(
                          <option key={index} value={item.category}>{item.category}</option>
                        )
                      })
                    }
                  </select>
                </div>
                <div className="col-sm-10 mt-3">
                  <label htmlFor="inputCategory">Trier par date</label>
                  <select id="inputCategory" className="form-select form-select-lg p-1">
                    <option defaultValue>Pas de filtre sélectionné</option>
                    <option value="plus">Croissant</option>
                    <option value="minus">Décroissant</option>
                  </select>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div className="col-12 col-lg-6 articles-preview-container d-flex flex-column align-items-center">
          {
            articles.map((article, index) => {
              return(
                <ArticlePreview key={index}
                  id={article.id}
                  title={article.title}
                  author={article.author}
                  description={article.description}
                  image={article.image_base64}
                  date={article.article_date_formated}
                  category={article.category}
                />
              )
            })
          }
        </div>
      </div>
    </div>
  )
}

export default ActualitesPage

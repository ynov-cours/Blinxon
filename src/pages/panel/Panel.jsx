import React, {useState, useEffect} from 'react';
import * as FIcons from 'react-icons/fa';
import config from '../../config.js';
import './panel.scss';

import NavbarPanel from '../../components/navbar-panel/NavbarPanel'

function Panel(props){

  const [articlesCount, setArticlesCount] = useState(0);
  const [commentsCount, setCommentsCount] = useState(0);

  useEffect(() => {
    updateArticlesCount();
    updateCommentsCount();
  }, []);

  const updateArticlesCount = () => {
    fetch(`${config.API_URL}/articles/all`)
    .then(response => response.json())
    .then(response => setArticlesCount(response.length));
  }

  const updateCommentsCount = () => {
    fetch(`${config.API_URL}/comments/all`)
    .then(response => response.json())
    .then(response => setCommentsCount(response.length));
  }

  // const updateCommentsCount = () => {
  //   fetch(`${config.API_URL}/comments/all`)
  //   .then(response => response.json())
  //   .then(response => setCommentsCount(response.length));
  // }

  return(
    <div id="panel">
      <NavbarPanel/>
      <div className="d-flex flex-column flex-lg-row col-12 justify-content-center justify-content-lg-around">
        <div className="panel-container col-12 d-flex flex-column flex-lg-row align-items-center align-items-lg-center justify-content-lg-around">
          <div className="panel-welcome-container d-flex flex-column justify-content-between p-4 col-9 col-lg-4">
            <div>
              <p className="panel-title p-0 m-0">Bonjour <span className="panel-title-names p-0 m-0">Olivia & Enzo</span></p>
              <p className="panel-welcome-text p-0 m-0">
                Bienvenue sur votre panel administrateur, depuis lequel vous pouvez modifier l'entièreté du contenu du site de BLINXON.
              </p>
            </div>
            <div className="d-flex flex-column">
              <div>
                <p className="panel-suggestions-title p-0 m-0 mb-3">Quelques suggestions :</p>
              </div>
              <div className="d-flex flex-wrap justify-content-between">
                <a className="panel-shortcut-button btn btn-lg btn-outline-success p-1" href="/panel/articles"><FIcons.FaPlus/> Ajouter un article</a>
                <a className="panel-shortcut-button btn btn-lg btn-outline-success p-1" href="/panel/text"><FIcons.FaEdit/> Modifier un texte</a>
                <a className="panel-shortcut-button btn btn-lg btn-outline-success p-1" href="/panel/comments"><FIcons.FaTrashAlt/> Supprimer un commentaire</a>
              </div>
            </div>
          </div>
          <div className="panel-stats-container d-flex flex-column justify-content-center col-9 col-lg-4">
            <p className="panel-stats-text">{articlesCount}<span className="panel-stats-text-legend"> articles postés</span></p>
          <p className="panel-stats-text">{commentsCount}<span className="panel-stats-text-legend"> commentaires</span></p>
            <p className="panel-stats-text">0<span className="panel-stats-text-legend"> sessions actives</span></p>
            <p className="panel-stats-text">0<span className="panel-stats-text-legend"> sessions au total</span></p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Panel;

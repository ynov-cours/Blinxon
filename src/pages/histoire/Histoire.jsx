import React from 'react';
import './histoire.scss';

function Histoire(props){
  return(
    <div id="histoire" className="d-flex flex-column">
      <div className="timeline-container d-flex flex-row align-items-center">
        <div className="timeline d-flex flex-wrap justify-content-around">
          {
            props.dates.map((item, index) =>
            item.reversed !== true ?
            (<div key={index} style={{transform:'translateY(-80%)'}} className="timeline-date-container d-flex flex-column align-items-center">
              <div className="text-center">
                <p className="timeline-text">{item.date}</p>
                <p className="timeline-text">{item.text}</p>
              </div>
              <div className="timeline-dot"></div>
            </div>)
            :
            (<div key={index} style={{transform:'translateY(-15%)'}} className="timeline-date-container d-flex flex-column-reverse  align-items-center">
              <div className="text-center">
                <p className="timeline-text">{item.date}</p>
                <p className="timeline-text">{item.text}</p>
              </div>
              <div className="timeline-dot"></div>
            </div>))
          }
        </div>
      </div>
      <div className="col-12 d-flex flex-column align-items-center">
        <div className="history-container col-11 d-flex flex-column align-items-around">
          {
            props.dates.map((item, index) =>
            item.reversed !== true ?
            (
              <div key={index} className="d-flex flex-row">
                <div className="timeline-explanations-container d-flex flex-column align-items-start mt-5 mb-5">
                  <p className="explanation-title">{item.date} - {item.text}</p>
                  <p className="explanation-text col-6">{item.description}</p>
                </div>
                <div className="dashed"></div>
              </div>
            )
            :
            (
              <div key={index} className="d-flex flex-row">
                <div className="dashed"></div>
                <div className="timeline-explanations-container d-flex flex-column align-items-end mt-5 mb-5">
                  <p className="explanation-title">{item.date} - {item.text}</p>
                  <p className="explanation-text col-6">{item.description}</p>
                </div>
              </div>
            )
          )
        }
      </div>
    </div>
  </div>
)
}

export default Histoire;

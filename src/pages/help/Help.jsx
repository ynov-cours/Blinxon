import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import {BsLink45Deg, BsDot} from 'react-icons/bs';
import './help.scss';

function Help(){
  return(
    <div id="help" className="d-flex flex-column align-items-center">
      <div className="col-12">
        <Navbar/>
      </div>
      <div className="col-11 d-flex flex-column pt-5 mb-5">
        <div>
          <p className="help-title">Pourquoi nous aider ?</p>
          <p>Le périple solidaire que nous allons entreprendre est aussi formidable qu’onéreux. <strong>Ainsi, pour le réaliser, nous allons avoir besoin de toute l’aide possible</strong>.</p>
          <p>Nous avons déjà à nos côtés plusieurs sponsors que nous remercions chaleureusement pour leurs généreux dons !</p>
          <p className="mt-2">Mais notre budget n’est pas encore clôturé et nous avons encore besoin de vous ! Que vous soyez un particulier ou une entreprise, nous tenons à souligner tout d’abord que chaque euro représente environ un kilomètre parcouru ! <strong>Il n’existe pas de petit don</strong> et chacun d’eux nous permet de nous rapprocher de notre objectif final.</p>

          <div className="col-12 d-flex flex-row justify-content-center">
            <div className="col-11">
              <p className="help-sponsor-title mt-4">Ils nous soutiennent déjà :</p>
              <div className="sponsors-container col-12 d-flex flex-wrap justify-content-center">
                <a className="m-2 p-3" href="https://www.apct-controle-technique-pelissanne.fr/" target="_blank" rel="noreferrer"><img src="/img/logos/sponsors/apct.png" alt="Logo entreprise APCT"/></a>
                <a className="m-2 p-3" href="https://www.montopoto.com/#deco-head" target="_blank" rel="noreferrer"><img src="/img/logos/sponsors/montopoto.png" alt="Logo parc d'attractions Montopoto"/></a>
                <a className="m-2 p-3" href="https://www.villagedesautomates.com/" target="_blank" rel="noreferrer"><img src="/img/logos/sponsors/village-automates.png" alt="Logo parc d'attractions Le Village des Automates"/></a>
                <a className="m-2 p-3" href="https://www.cultura.com/" target="_blank" rel="noreferrer"><img src="/img/logos/sponsors/cultura.png" alt="Logo magasin Cultura"/></a>
                <a className="m-2 p-3" href="https://www.privilegegourmand.fr/" target="_blank" rel="noreferrer"><img src="/img/logos/sponsors/privilege-gourmand.png" alt="Logo entreprise Privilège Gourmand"/></a>
                <a className="m-2 p-3" href="http://www.societemorbelli.com/" target="_blank" rel="noreferrer"><img src="/img/logos/sponsors/smf.png" alt="Logo entreprise Société Morbelli Frères"/></a>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-5 mb-3">
          <p className="help-title">Comment nous aider ?</p>
          <p><BsDot/><strong>Si vous êtes un particulier</strong> et que vous souhaitez apporter votre soutien à notre projet, une cagnotte en ligne est d’ores et déjà en place. Elle est accessible en cliquant <a className="green" href="/">ici <BsLink45Deg/></a>. Vous pouvez également directement faire un don via le dispositif PayPal ci-dessous.</p>
          <div className="col-12 d-flex flex-column align-items-center mb-3">
            <div className="paypal my-4 p-4 text-center">
              <p className="mb-3">Faites un don en cliquant sur le bouton ci-dessous :</p>
              <form className="paypal-form" action="https://www.paypal.com/donate" method="post" target="_blank" rel="noreferrer">
              <input type="hidden" name="hosted_button_id" value="NFNG63PQH9NNJ"/>
              <input type="image" src="https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button"/>
              <img alt="" border="0" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" width="1" height="1"/>
            </form>
          </div>
        </div>
        <p><BsDot/><strong>Si vous êtes une entreprise</strong> et que le sponsoring de notre équipage vous intéresse, nous vous invitons à prendre contact avec nous via notre <a href="/nous-contacter" className="green">formulaire de contact <BsLink45Deg/></a> afin que nous convenions ensemble des dispositions du contrat.</p>
        <p>En effet, en échange de dons financiers, nous nous engageons à faire la publicité de votre entreprise sur nos réseaux sociaux, notamment Instagram qui comptabilise à ce jour plus de 750 abonnés. De plus, nous vous proposons d’apposer des stickers à l’effigie de votre entreprise sur notre 4L. Numéro de téléphone, logo, slogan, tout est possible !</p>
        <p className="mt-2">Nous vous invitons à consulter notre dossier de sponsoring téléchargeable <a download="DOSSIER SPONSORING - BLINXON - 4L TROPHY 2022" href="/utils/dossier_sponso.pdf" className="green">ici <BsLink45Deg/></a>. Vous trouverez toutes les informations nécessaires afin de répondre à vos premiers questionnements.</p>
      </div>
    </div>
  </div>
)
}

export default Help;

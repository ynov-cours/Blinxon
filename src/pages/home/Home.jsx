import React, { useEffect, useState } from 'react';
import config from '../../config.js';
import './home.scss';
import PagePanel from '../../components/page-panel/PagePanel';
import LivePanel from '../../components/live-panel/LivePanel';

function Home(props){

  const [livePanel, setLivePanel] = useState([]);

  useEffect(() => {
    getLivePanel();
  }, [])

  const getLivePanel = () => {
    fetch(`${config.API_URL}/panels/live`)
    .then(response => response.json())
    .then(response => setLivePanel(response));
  }

  return(
    <div id="home" className="d-flex flex-column align-items-center">
      <div className="blinxon-page-panel-container col-11 col-lg-8 d-flex flex-column align-items-center justify-content-center mt-5">
        <div className="col-12 p-0 d-flex flex-row justify-content-center">
          {
            livePanel.length ?
            livePanel.map((item, index) => {
              return(
                <LivePanel panel_image={item.image_base64} panel_id="0" panel_title={item.title} panel_subtitle={item.subtitle} panel_mode="large"/>
              )
            })
            :
            <PagePanel panel_image="/panels/default_panel_image_not_blured.png" panel_id="0" panel_title="Bienvenue sur le site de Blinxon !" panel_subtitle="Rejoignez-nous dans une aventure humaine et sportive incroyable." panel_mode="large"/>
          }
        </div>
        <div className="col-12 p-0 d-flex flex-wrap justify-content-between">
          <PagePanel
            panel_title="Notre histoire"
            panel_mode="normal"
            panel_image="template_little.png"
            panel_link="/notre-histoire"
          />

          <PagePanel
            panel_title="Statistiques du projet"
            panel_mode="normal"
            panel_image="template_little.png"
            panel_link="/nos-statistiques"
          />

          <PagePanel
            panel_title="Nos actualités"
            panel_mode="normal"
            panel_image="template_little.png"
            panel_link="/nos-actualites"
          />

        </div>
      </div>
    </div>
  )
}

export default Home;

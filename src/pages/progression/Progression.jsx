import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import StatBar from '../../components/stat-bar/StatBar';
import './progression.scss';

function Progression(props){
  return(
    <div id="progress" className="d-flex flex-column align-items-center">
      <div className="col-12 p-0 m-0">
        <Navbar/>
      </div>
      <div className="col-12 col-lg-11 p-0 m-0 pt-5">
        <div className="stats-intro mb-5">
          <p className="stats-intro-title">Suivre notre progression</p>
          <p className="stats-intro-text">Nous vous proposons de suivre l'avancement de notre projet et de notre préparation pour le raid 4L Trophy au travers de ces différentes statistiques, mises a jours en quasi temps réel !</p>
        </div>
        <div className="stats-container col-12 col-lg-11 d-flex flex-column align-items-center">
          <StatBar
            title="Départ du 4L Trophy"
            description="Le départ du 4L Trophy est le 17 février 2022 ! Il nous reste encore pas mal de temps avant cette date, pour être prêts et partir serainement !"
            percentage="18"
            min="0"
            max="6000"
            unit="Km"
            links_name={["Link 1", "Link 2", "Link 3"]}
            links_url={["/", "/nous-aider", "/"]}
            color="#0F7322"
          />

          <StatBar
            title="Rassemblement des dons pour Enfants du Désert"
            description="Le départ du 4L Trophy est le 17 février 2022 ! Il nous reste encore pas mal de temps avant cette date, pour être prêts et partir serainement !"
            percentage="70"
            min="0"
            max="20"
            unit="Kg"
            links_name={["Link 1", "Link 2"]}
            links_url={["/", "/nous-aider"]}
            color="#3a4dc1"
          />

          <StatBar
            title="Mécanique de la voiture"
            description="Le départ du 4L Trophy est le 17 février 2022 ! Il nous reste encore pas mal de temps avant cette date, pour être prêts et partir serainement !"
            percentage="55"
            min="0"
            max="144"
            unit="pièces"
            links_name={["Link 1"]}
            links_url={["/"]}
            color="#fae848"
          />

        </div>
      </div>
    </div>
  )
}

export default Progression;

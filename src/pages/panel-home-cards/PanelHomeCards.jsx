import React, { useEffect, useState } from 'react';
import NavbarPanel from '../../components/navbar-panel/NavbarPanel';
import LivePanel from '../../components/live-panel/LivePanel';
import config from '../../config';
import * as FIcons from 'react-icons/fi';
import axios from 'axios';
import './panelhomecards.scss';

function PanelHomeCards(props){

  const [livePanels, setLivePanels] = useState([]);
  const [selectedLivePanel, setSelectedLivePanel] = useState([]);

  useEffect(() => {
    updateLivePanelsList();
  }, []);

  const updateLivePanelsList = () => {
    fetch(`${config.API_URL}/panels/all`)
    .then((response) => response.json())
    .then(response => setLivePanels(response));
  }

  const handleClickArticlePreview = (id) => {
    fetch(`${config.API_URL}/panels/${id}`)
    .then((response) => response.json())
    .then(response => setSelectedLivePanel(response));

    document.querySelectorAll('.panel-homecards-card').forEach((item) => {
      item.classList.remove('panel-homecards-selected-card');
      // console.log("test");
    });

    let selectedCard = document.getElementById(`panel-homecards-card-${id}`);
    if(selectedCard.classList.contains("panel-homecards-selected-card")){
      selectedCard.classList.remove('panel-homecards-selected-card');
    }else{
      selectedCard.classList.add('panel-homecards-selected-card');
    }
  }

  const deletePanel = (id) => {
    axios.post(`${config.API_URL}/panels/delete/${id}?token=${config.TOKEN}`)
    .then(function(response){
      fetch(`${config.API_URL}/panels/live`)
      .then((response) => response.json())
      .then(response => setSelectedLivePanel(response));
      updateLivePanelsList();
    })
    .catch(function(err){
      // Do stuff when error
      console.log("Une erreur s'est produite...");
      console.log(err);
    });
  }

  return(
    <div id="panelhomecards" className="d-flex flex-column align-items-center">
      <div className="container-fluid">
        <NavbarPanel/>
      </div>
      <div className="d-flex flex-column col-11">
        <div className="panel-homecards-title-container pt-5">
          <p className="panel-homecards-title">Administration : Live panels</p>
        </div>
        <div className="panel-homecards-content-container d-flex flex-column flex-lg-row pt-5">
          <div className="panel-homecards-list-container col-12 col-lg-3">
            {
              livePanels.length > 0 ?
              livePanels.map(function(panel, index){
                return(
                  <div onClick={() => handleClickArticlePreview(panel.id)} key={index} id={`panel-homecards-card-${panel.id}`} className={`panel-homecards-card d-flex flex-row p-2`}>
                    <div className="col-8 d-flex flex-column">
                      <p className="panel-homecards-card-title"><strong>{panel.title}</strong></p>
                      <p className="panel-homecards-card-date">id:{panel.id}</p>
                    </div>
                    <div className="col-4 d-flex flex-row align-items-center justify-content-center">
                      <a href={`/panel/live-panel/edit/${panel.id}`} className="panel-homecards-card-button mx-1 green"><FIcons.FiEdit/></a>
                      <button onClick={() => deletePanel(panel.id)} className="panel-homecards-card-button mx-1 red"><FIcons.FiTrash2/></button>
                    </div>
                  </div>
                )
              })
              :
              <div className="d-flex flex-column justify-content-center align-items-center" style={{height: "100%"}}>
                <p>Aucun panels trouvé.</p>
                <button onClick={() => updateLivePanelsList()} className="btn btn-outline-success p-1 mt-2">Réessayer</button>
                <a href={`/panel/live-panel/add`} className="panel-homecards-list-button btn btn-lg btn-outline-success p-1 mt-2"><FIcons.FiPlus/> Ajouter un live panel</a>
              </div>
            }

            {
              livePanels.length > 0 ?
              <div className="col-12 d-flex flex-row justify-content-center mt-2 mb-2">
                <a href={`/panel/live-panel/add`} className="panel-homecards-list-button btn btn-lg btn-outline-success p-1"><FIcons.FiPlus/> Ajouter un live panel</a>
              </div>
              :
              ""
            }
          </div>
          <div className="panel-homecards-preview-container ps-5 col-9">
            <p className="panel-homecards-preview-title panel-homecards-title">Panel actuel :</p>
            {
              selectedLivePanel
              ?
              <LivePanel
                panel_id={selectedLivePanel.id}
                panel_title={selectedLivePanel.title}
                panel_mode="large"
                panel_subtitle={selectedLivePanel.subtitle}
                panel_image={selectedLivePanel.image_base64}
              />
              :
              <p>Aucun panel sélectionné.</p>
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default PanelHomeCards;

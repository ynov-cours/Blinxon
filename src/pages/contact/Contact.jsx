import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import ReactMarkdown from 'react-markdown';
import gfm from 'remark-gfm';
import emoji from 'remark-emoji';
import { FiInstagram, FiFacebook, FiTwitter, FiDownload } from "react-icons/fi";
import './contact.scss';

function Contact(){
  return(
    <div id="contact" className="d-flex flex-column align-items-center">
      <div className="col-12">
        <Navbar/>
      </div>
      <div className="col-11 pt-5 d-flex flex-column">
        <div className="contact-intro col-12 border-bottom pb-2">
          <ReactMarkdown className="contact-title" plugins={[gfm, emoji]} children=":phone: Nous contacter"/>
          <p className="contact-text">Vous êtes une entreprise, une autre association ou juste un particulier et vous souhaitez nous contacter ?</p>
          <p className="contact-text">Vous trouverez sur cette page toutes les informations dont vous avez besoin !</p>
        </div>
        <div className="col-12 d-flex flex-column flex-lg-row justify-content-between pt-3">
          <div className="col-12 col-lg-3 mb-5">
            <div className="contact-form-container p-4">
              <ReactMarkdown className="contact-form-title mb-3" plugins={[gfm, emoji]} children="Formulaire de contact"/>
              {/* <ReactMarkdown className="contact-form-title mb-3" plugins={[gfm, emoji]} children=":envelope: Formulaire de contact"/> */}
              <form>
                <div class="mb-3">
                  <label for="inputName" class="form-label">Votre nom <span className="red">*</span></label>
                  <input type="email" class="form-control p-1" id="inputName" placeholder="Votre nom et prénom"/>
                </div>
                <div class="mb-3">
                  <label for="inputCompany" class="form-label">Votre société</label>
                  <input type="email" class="form-control p-1" id="inputCompany" placeholder="Le nom de votre société"/>
                </div>
                <div class="mb-3">
                  <label for="inputMail" class="form-label">Votre email <span className="red">*</span></label>
                  <input type="email" class="form-control p-1" id="inputMail" placeholder="Votre email"/>
                </div>
                <div class="mb-3">
                  <label for="inputMessageObject" class="form-label">Sujet de votre message <span className="red">*</span></label>
                  <input type="email" class="form-control p-1" id="inputMessageObject" placeholder="De quoi voulez-vous nous parler ?"/>
                </div>
                <div class="mb-3">
                  <label for="inputMessageObject" class="form-label">Sujet de votre message <span className="red">*</span></label>
                  <textarea className="form-control p-1" id="inputContent" rows="4" placeholder="Contenu de votre message"></textarea>
                </div>
                <div class="row col-12">
                  <div class="col-7 ">
                    <label for="formFileMultiple" class="form-label">Des fichiers à nous transmettre ?</label>
                    <input class="form-control p-1" type="file" id="formFileMultiple" multiple/>
                  </div>
                  <div className="col-5 d-flex flex-row justify-content-end align-items-end">
                    <button style={{fontSize:".8em"}} type="submit" class="btn btn-outline-success p-1">Envoyer mon message</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div className="col-12 col-lg-3 mb-5 mx-5">
            <div className="contact-form-container p-4">
              <ReactMarkdown className="contact-social-title" plugins={[gfm, emoji]} children="Nos réseaux"/>
              {/* <ReactMarkdown className="contact-social-title" plugins={[gfm, emoji]} children=":loudspeaker: Nos réseaux"/> */}

              <div className="d-flex flex-column justify-content-around">
                <a href="/" rel="noref nopenner" target="_blank" className="col-12 contact-social-card p-3 mb-3 d-flex flex-column">
                  <div className="d-flex flex-row">
                    <p className="social-card-title"><FiInstagram/></p>
                    <p className="social-card-title ms-2">Instagram</p>
                  </div>
                  <div className="d-flex flex-row justify-content-between">
                    <p className="social-card-infos">764 Abonnés</p>
                    <p className="social-card-infos">27 Posts</p>
                  </div>
                </a>

                <a href="/" rel="noref nopenner" target="_blank" className="col-12 contact-social-card p-3 mb-3 d-flex flex-column border-bottom border-top">
                  <div className="d-flex flex-row">
                    <p className="social-card-title"><FiTwitter/></p>
                    <p className="social-card-title ms-2">Twitter</p>
                  </div>
                  <div className="d-flex flex-row justify-content-between">
                    <p className="social-card-infos">6 Abonnés</p>
                    <p className="social-card-infos">12 Tweets</p>
                  </div>
                </a>

                <a href="/" rel="noref nopenner" target="_blank" className="col-12 contact-social-card p-3 mb-3 d-flex flex-column">
                  <div className="d-flex flex-row">
                    <p className="social-card-title"><FiFacebook/></p>
                    <p className="social-card-title ms-2">Facebook</p>
                  </div>
                  <div className="d-flex flex-row justify-content-between">
                    <p className="social-card-infos">3 Abonnés</p>
                    <p className="social-card-infos">27 Posts</p>
                  </div>
                </a>
              </div>
            </div>
          </div>

          <div className="col-12 col-lg-3 mb-5 mx-5">
            <div className="contact-form-container p-4">
              <ReactMarkdown className="contact-utils-title" plugins={[gfm, emoji]} children="Nos documents"/>
              {/* <ReactMarkdown className="contact-utils-title" plugins={[gfm, emoji]} children=":file_folder: Nos documents"/> */}

              <div className="d-flex flex-column justify-content-around">
                <a href="/" download="" className="col-12 contact-utils-card p-3 mb-3 d-flex flex-column">
                  <div className="d-flex flex-row">
                    <p className="utils-card-title"><FiDownload/></p>
                    <p className="utils-card-title ms-2">Dossier de sponsoring</p>
                  </div>
                  <div className="d-flex flex-row justify-content-start">
                    <p className="utils-card-infos">24 Téléchargements</p>
                  </div>
                </a>

                <a href="/" download="" className="col-12 contact-utils-card p-3 mb-3 d-flex flex-column border-bottom border-top">
                  <div className="d-flex flex-row">
                    <p className="utils-card-title"><FiDownload/></p>
                    <p className="utils-card-title ms-2">Tarifs encarts publicitaires</p>
                  </div>
                  <div className="d-flex flex-row justify-content-start">
                    <p className="utils-card-infos">55 Téléchargements</p>
                  </div>
                </a>

                <a href="/" download="" className="col-12 contact-utils-card p-3 mb-3 d-flex flex-column">
                  <div className="d-flex flex-row">
                    <p className="utils-card-title"><FiDownload/></p>
                    <p className="utils-card-title ms-2">Flyer édition 2022</p>
                  </div>
                  <div className="d-flex flex-row justify-content-start">
                    <p className="utils-card-infos">3 Téléchargements</p>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Contact;

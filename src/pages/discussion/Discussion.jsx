import React, {useState, useEffect} from 'react';
import Navbar from '../../components/navbar/Navbar';
import Comment from '../../components/comment/Comment';
import {FaSignature} from 'react-icons/fa';
import config from '../../config';
import axios from 'axios';
import './discussion.scss';

function Discussion(props){

  const [comments, setComments] = useState([]);

  const [author, setAuthor] = useState("Anonyme");
  const [message, setMessage] = useState("");

  useEffect(() => {
    getComments();
  }, []);

  const getComments = () => {
    fetch(`${config.API_URL}/comments/all`)
    .then(response => response.json())
    .then(response => setComments(response));
  }

  const handleSubmit = (e) => {
    e.preventDefault();

    let comment = {
      author: author,
      comment: message
    }

    axios.post(`${config.API_URL}/comments/add`, comment)
    .then(function(response){
      let feedback = document.getElementById('comment-feedback');
      feedback.style.color = "#0F7322";
      feedback.innerHTML = "Message envoyé";
      getComments();
      document.getElementById('comment-post-form').reset();
      setAuthor("Anonyme");
      setTimeout(() => {
        feedback.innerHTML = "";
      }, 5000);
    }).catch(function(err){
      let feedback = document.getElementById('comment-feedback');
      feedback.style.color = "#e74c3c";
      feedback.innerHTML = "Erreur...";
      getComments();
      // setAuthor("Anonyme");
      setTimeout(() => {
        feedback.innerHTML = "";
      }, 5000);
    });
  }

  return(
    <div id="discussion" className="d-flex flex-column align-items-center">
      <div className="col-12">
        <Navbar/>
      </div>
      <div className="d-flex flex-column col-11 pt-3">
        <div className="d-flex flex-column align-items-center border-bottom pb-1 pb-3 mb-3">
          <p className="comments-title"><FaSignature/> Livre d'Or</p>
          <p className="comments-subtitle text-center">Ici vous pouvez laisser un message, une remarque, une suggestion ou un petit mot d'encouragement pour l'équipage Blinxon.</p>
        </div>
        <div className="comments-container d-flex flex-wrap justify-content-center">
          <div className="comment-form col-12 col-lg-3 p-2 mt-3 mx-3 d-flex flex-column align-items-center justify-content-between">
            {/* <p className="comment-form-title">Vous aussi, signez le livre d'Or !</p> */}
            <form id="comment-post-form" className="col-12" onSubmit={handleSubmit}>
              <div className="d-flex flex-column">
                <div className="col-12 p-1">
                  <div className="overflow-auto">
                    <label for="inputMessage" className="form-label">Vous aussi, signez le livre d'Or ! <span className="red">*</span></label>
                    <textarea required onChange={(e) => setMessage(e.target.value)} style={{resize: "none"}} className="form-control p-1" id="inputMessage" rows="3" placeholder="Bonjour ! Écrivez votre message dans ce cadre."></textarea>
                    <small className="p-0 m-0" style={{opacity: ".5"}}>Ne divulguez <strong>jamais</strong> vos informations personnelles</small>
                  </div>
                  <div className="col-12 d-flex flex-row justify-content-center align-items-end">
                    <div className="col-8">
                      <label for="inputPseudo" className="form-label">Votre nom <small>(Anonyme si laissé vide)</small></label>
                      <input onChange={(e) => setAuthor(e.target.value)} type="text" className="form-control p-1" id="inputPseudo" placeholder="Blinxon"/>
                    </div>
                    <div className="col-4 d-flex flex-row align-items-center">
                      <button type="submit" className="btn btn-outline-success ms-3 p-1">Envoyer</button>
                      <p id="comment-feedback" className="ms-2"></p>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>

          {
            comments.map((comment, index) => {
              return(
                <Comment
                  text={comment.comment}
                  date={comment.comment_date_formated}
                  author={comment.author}
                  approuved={comment.is_approuved}
                />
              )
            })
          }
        </div>
      </div>
    </div>
  )
}

export default Discussion;

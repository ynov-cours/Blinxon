import React, { useEffect, useState } from 'react';
import NavbarPanel from '../../components/navbar-panel/NavbarPanel';
import config from '../../config';
import * as FIcons from 'react-icons/fi';
import ReactMarkdown from 'react-markdown';
import gfm from 'remark-gfm';
import emoji from 'remark-emoji';
// import remark from 'remark';
import axios from 'axios';
import './panelarticles.scss';

function PanelArticles(props){

  const [articles, setArticles] = useState([]);
  const [selectedArticle, setSelectedArticle] = useState([]);

  useEffect(() => {
    updateArticlesList();
  }, []);

  const updateArticlesList = () => {
    fetch(`${config.API_URL}/articles/all`)
    .then((response) => response.json())
    .then(response => setArticles(response));
  }

  const handleClickArticlePreview = (id) => {
    fetch(`${config.API_URL}/articles/${id}`)
    .then((response) => response.json())
    .then(response => setSelectedArticle(response));

    document.querySelectorAll('.panel-article-card').forEach((item) => {
      item.classList.remove('panel-articles-selected-card');
      // console.log("test");
    });

    let selectedCard = document.getElementById(`panel-article-card-${id}`);
    if(selectedCard.classList.contains("panel-articles-selected-card")){
      selectedCard.classList.remove('panel-articles-selected-card');
    }else{
      selectedCard.classList.add('panel-articles-selected-card');
    }
  }

  const deleteArticle = (id) => {
    axios.post(`${config.API_URL}/articles/delete/${id}?token=${config.TOKEN}`)
    .then(function(response){
      updateArticlesList();
    })
    .catch(function(err){
      // Do stuff when error
      console.log("Une erreur s'est produite...");
      console.log(err);
    });
  }

  return(
    <div id="panelarticles" className="d-flex flex-column align-items-center">
      <div className="container-fluid">
        <NavbarPanel/>
      </div>
      <div className="d-flex flex-column col-11">
        <div className="panel-articles-title-container pt-5">
          <p className="panel-articles-title">Administration : Articles</p>
        </div>
        <div className="panel-articles-content-container d-flex flex-column flex-lg-row pt-5">
          <div className="panel-articles-list-container col-12 col-lg-3">
            {
              articles.length > 0 ?
              articles.map(function(article, index){
                return(
                  <div onClick={() => handleClickArticlePreview(article.id)} key={index} id={`panel-article-card-${article.id}`} className={`panel-article-card d-flex flex-row p-2`}>
                    <div className="col-8 d-flex flex-column">
                      <strong><ReactMarkdown className="panel-article-card-title" plugins={[gfm, emoji]} children={article.title}/></strong>
                      <p className="panel-article-card-date">{article.article_date_formated} - id:{article.id}</p>
                    </div>
                    <div className="col-4 d-flex flex-row align-items-center justify-content-center">
                      <a href={`/panel/articles/edit/${article.id}`} className="panel-article-card-button mx-1 green"><FIcons.FiEdit/></a>
                      <button onClick={() => deleteArticle(article.id)} className="panel-article-card-button mx-1 red"><FIcons.FiTrash2/></button>
                    </div>
                  </div>
                )
              })
              :
              <div className="d-flex flex-column justify-content-center align-items-center" style={{height: "100%"}}>
                <p>Aucun article trouvé.</p>
                <button onClick={() => updateArticlesList()} className="btn btn-outline-success p-1 mt-2">Réessayer</button>
                <a href={`/panel/articles/add`} className="panel-article-list-button btn btn-lg btn-outline-success p-1 mt-2"><FIcons.FiPlus/> Ajouter un article</a>
              </div>
            }

            {
              articles.length > 0 ?
              <div className="col-12 d-flex flex-row justify-content-center mt-2 mb-2">
                <a href={`/panel/articles/add`} className="panel-article-list-button btn btn-lg btn-outline-success p-1"><FIcons.FiPlus/> Ajouter un article</a>
              </div>
              :
              ""
            }
          </div>
          <div className="panel-article-preview-container ps-5">
            <p className="panel-article-preview-title panel-articles-title">Article actuel :</p>
            <hr/>
            {
              selectedArticle !== ""
              ?
              <div>
                <ReactMarkdown className="panel-articles-title" plugins={[gfm, emoji]} children={selectedArticle.title}/>
                <p className="panel-articles-infos">Posté par {selectedArticle.author} le {selectedArticle.article_date_formated}</p>
                <div className="d-flex flex-row justify-content-start">
                  <img className="panel-article-image" src={`data:image/plain;base64,${selectedArticle.image_base64}`} alt={`Illustration de l'article n°${selectedArticle.id}`}/>
                </div>
                <ReactMarkdown plugins={[gfm, emoji]} className="mt-5 panel-article-content" children={selectedArticle.content}/>
              </div>
              :
              <p className="mt-2">Aucun article sélectionné.</p>
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default PanelArticles;

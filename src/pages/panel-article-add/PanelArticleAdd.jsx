import React, { useState, useEffect } from 'react';
import NavbarPanel from '../../components/navbar-panel/NavbarPanel';
import ReactMarkdown from 'react-markdown';
import gfm from 'remark-gfm';
import axios from 'axios';
import config from '../../config.js';
// import '../panel-article-edit/panelarticlesedit.scss';

function PanelArticlesAdd(props){

  const [existingArticles, setExistingArticles] = useState([]);
  const [title, setTitle] = useState("");
  const [author, setAuthor] = useState("");
  const [description, setDescription] = useState("");
  const [content, setContent] = useState("");
  const [category, setCategory] = useState("association");
  const [image, setImage] = useState(null)

  useEffect(() => {
    updateArticlesList();
  }, []);

  const updateArticlesList = () => {
    fetch(`${config.API_URL}/articles/all`)
    .then(response => response.json())
    .then(response => setExistingArticles(response));
  }

  const addArticle = (e) => {
    e.preventDefault();

    // let article = {
    //   title: title,
    //   author: author,
    //   description: description,
    //   content: content,
    //   category: category,
    //   image: image
    // }

    let article = new FormData();
    article.append("title", title);
    article.append("author", author);
    article.append("description", description);
    article.append("content", content);
    article.append("category", category);
    article.append("image", image);

    axios.post(`${config.API_URL}/articles/add?token=${config.TOKEN}`, article, {headers: {content: "multipart/form-data"}})
    .then(function(response){
      // Success stuff
      let feedback = document.getElementById('add_feedback');
      feedback.style.color = "#0F7322";
      feedback.innerHTML = "Article ajouté avec succès !";
      updateArticlesList();
      document.getElementById('add_article_form').reset();

      setTimeout(function(){
        feedback.innerHTML = "";
      },5000);
    })
    .catch(function(err){
      // Error stuff
      let feedback = document.getElementById('add_feedback');
      feedback.style.color = "#e74c3c";
      feedback.innerHTML = "Une erreur est survenue...";
      updateArticlesList();

      setTimeout(function(){
        feedback.innerHTML = "";
      },5000);
    })
  }

  const escapeHTML = (str) => {
    return str.replace( /[\u00A0-\u9999<>]/gim, function( i) { return  '&'; });
  }


  return(
    <div id="panelarticlesedit" className="d-flex flex-column">
      <div>
        <NavbarPanel/>
      </div>
      <div className="d-flex flex-column flex-lg-row">
        <div className="panel-edit-form-container col-12 col-lg-3 p-3 px-5 border-end">
          <p className="panel-edit-article-title">Panel d'ajout</p>
          <form id="add_article_form" onSubmit={addArticle}>
            <div className="form-group mt-3">
              <label htmlFor="exampleInputEmail1">Titre</label>
              <input required onChange={(e) => setTitle(e.target.value)} type="text" className="form-control p-1" id="inputTitle" placeholder="Titre de l'article"/>
            </div>
            <div className="form-group mt-3">
              <label htmlFor="inputAuthor">Auteur</label>
              <input required onChange={(e) => setAuthor(e.target.value)} type="text" className="form-control p-1" id="inputAuthor" placeholder="Auteur de l'article"/>
            </div>
            <div className="form-group mt-3">
              <label htmlFor="inputDescription">Description</label>
              <input required onChange={(e) => setDescription(e.target.value)} type="text" className="form-control p-1" id="inputDescription" placeholder="Description de l'article"/>
            </div>
            <div className="form-group mt-3">
              <label htmlFor="inputContent">Contenu</label>
              <textarea required onChange={(e) => setContent(e.target.value)} className="form-control p-1" id="inputContent" rows="4" placeholder="Contenu de l'article"></textarea>
            </div>
            <small className="p-0 m-0">Pour un retour a la ligne utiliser : <code className="p-0 m-0">{escapeHTML("&nbsp;")}</code> avec 2 espaces apres.</small>
            <form>
              <div className="form-group mt-3">
                <label for="inputImage">Image d'illustration</label>
                <input onChange={(e) => setImage(e.target.files[0])} type="file" className="form-control-file" id="inputImage"/>
              </div>
            </form>
            <div className="form-group mt-3">
              <label htmlFor="inputContent">Categorie</label>
              <input required onChange={(e) => setCategory(e.target.value)} type="text" className="form-control p-1" id="inputCatégory" placeholder="Catégorie de l'article"/>
            </div>
            <button type="submit" className="btn btn-outline-success mt-3 p-1">Ajouter l'article</button>
            <p id="add_feedback"></p>
          </form>

          <div className="mt-3">
            <p><strong>Liste des articles déjà existants :</strong></p>
            <div className="existing-articles-container">
              {
                existingArticles.length > 0
                ?
                existingArticles.slice(0).reverse().map((item, index) => {
                  return(
                    <a key={index} className="blinxon-link" href={`/article/${item.id}`}><li>{item.title} - <small>{item.article_date_formated}</small></li></a>
                  )
                })
                :
                "Aucun article"
              }
            </div>
          </div>
        </div>
        <div className="panel-edit-article-preview-container col-12 col-lg-8 p-3">
          <p className="panel-edit-article-title-preview">{title !== "" ? title : "Pas de titre"}</p>
          {author !== "" ? <p className="panel-edit-article-subline-preview mb-3">Posté par <strong>{author}</strong></p> : <p className="panel-edit-article-subline-preview mb-3">Posté par <strong>[Aucun auteur mentioné]</strong></p>}

          {content !== "" ? <ReactMarkdown className="panel-edit-article-content-preview" plugins={[gfm]} children={content}/> : <ReactMarkdown plugins={[gfm]} className="panel-edit-article-content-preview" children="Aucun contenu"/>}
        </div>
      </div>
    </div>
  )
}

export default PanelArticlesAdd;

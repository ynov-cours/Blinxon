import React, { useState, useEffect } from 'react';
import NavbarPanel from '../../components/navbar-panel/NavbarPanel';
import ReactMarkdown from 'react-markdown';
import gfm from 'remark-gfm';
import config from '../../config.js';
import './panelarticlesedit.scss';

function PanelArticlesEdit(props){

  const [article, setArticle] = useState([]);
  const [updatedTitle, setUpdatedTitle] = useState("");
  const [updatedAuthor, setUpdatedAuthor] = useState("");
  const [updatedContent, setUpdatedContent] = useState("");

  useEffect(() => {
    fetch(`${config.API_URL}/articles/${props.match.params.id}`)
    .then((response) => response.json())
    .then((response) => setArticle(response));
  }, [props.match.params.id]);

  return(
    <div id="panelarticlesedit" className="d-flex flex-column">
      <div>
        <NavbarPanel/>
      </div>
      <div className="d-flex flex-column flex-lg-row">
        <div className="panel-edit-form-container col-12 col-lg-3 p-3 px-5 border-end">
          <p className="panel-edit-article-title">Panel d'édition</p>
          <form>
            <div class="form-group mt-3">
              <label for="exampleInputEmail1">Titre</label>
              <input onChange={(e) => setUpdatedTitle(e.target.value)} type="text" class="form-control" id="inputTitle" placeholder={article.title}/>
            </div>
            <div class="form-group mt-3">
              <label for="inputAuthor">Auteur</label>
              <input onChange={(e) => setUpdatedAuthor(e.target.value)} type="text" class="form-control" id="inputAuthor" placeholder={article.author}/>
            </div>
            <div class="form-group mt-3">
              <label for="inputContent">Contenu</label>
              <textarea onChange={(e) => setUpdatedContent(e.target.value)} class="form-control" id="inputContent" rows="8" placeholder={article.content}></textarea>
            </div>
            <button type="submit" class="btn btn-outline-success mt-3 p-1">Mettre à jour l'article</button>
          </form>
        </div>
        <div className="panel-edit-article-preview-container col-12 col-lg-8 p-3">
          <p className="panel-edit-article-title-preview">{updatedTitle !== "" ? updatedTitle : article.title}</p>
          {updatedAuthor !== "" ? <p className="panel-edit-article-subline-preview mb-3">Posté par <strong>{updatedAuthor}</strong></p> : <p className="panel-edit-article-subline-preview mb-3">Posté par <strong>{article.author}</strong></p>}

          {updatedContent !== "" ? <ReactMarkdown className="panel-edit-article-content-preview" plugins={[gfm]} children={updatedContent}/> : <ReactMarkdown plugins={[gfm]} className="panel-edit-article-content-preview" children={article.content}/>}
        </div>
      </div>
    </div>
  )
}

export default PanelArticlesEdit;
